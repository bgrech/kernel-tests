# storage/nvme_rdma/nvmeof_rdma_connect_with_keep_alive_tmo_1

Storage: nvmeof rdma connect with --keep-alive-tmo=1

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
