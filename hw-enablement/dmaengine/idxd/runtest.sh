#!/bin/bash
# -*- mode: Shell-script; sh-shell: bash; sh-basic-offset: 4; sh-indentation: 4; coding: utf-8-unix; indent-tabs-mode: t; ruler-mode-show-tab-stops: t; tab-width: 4 -*-
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Copyright (c) 2024 Red Hat, Inc
#
# SPDX-License-Identifier: GPL-2.0-or-later
#
# Summary: Test idxd user workqueue with dsa_config_test_runner.sh
#
# Author: Jerry Snitselaar <jsnitsel@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

FILE=$(readlink -f ${BASH_SOURCE[0]})
CDIR=$(dirname $FILE)

DMA_MODNAME="idxd"

if test -z "${DMA_IOMMU_CONF}" -o -z "${IDXD_ACCFG_TEST}"; then
	test_complete "${DMA_TESTNAME}" "SKIP" "requires the DMA_IOMMU_CONF and IDXD_ACCFG_TEST variables to be set"
fi

DMA_TESTNAME="${DMA_MODNAME}-${DMA_IOMMU_CONF}-${IDXD_ACCFG_TEST}"

. "/usr/share/beakerlib/beakerlib.sh" || exit 1
. "${CDIR}/../../../iommu/include/iommu-helper.sh" || exit 1
. "${CDIR}/../include/dmaengine-helper.sh" || exit 1
. "${CDIR}/../include/dsa-helper.sh" || exit 1

if ! test -z "${DMAENG_DEBUG}"; then
	set -x
fi

if ! test -d "${DMA_STATEDIR}"; then
	intel_iommu_supported
	init_state
	accel_config_test_install
	set_config "${DMA_IOMMU_CONF}"
	set_phase "config"
fi

if check_phase "config"; then
	grub_setup "$(get_config)"
fi

case "${IDXD_ACCFG_TEST}" in
	"dsa_config_test_runner" | "dsa_user_test_runner")
		idxd_check
		;;
	"iaa_user_test_runner")
		idxd_check
		iaa_check
		;;
	*)
		test_complete "${DMA_TESTNAME}" "SKIP" "unknown IDXD test name: ${IDXD_ACCFG_TEST}"
		;;
esac

if ! is_intel_sm_supported; then
	test_complete "${DMA_TESTNAME}" "SKIP" "scalable mode is not supported"
fi

if ! intel_iommu_enabled; then
	test_complete "${DMA_TESTNAME}" "FAIL" "IOMMU didn't initialize"
fi

iommu_state_check

if check_phase "reboot"; then
	set_phase "run"
fi

if ! check_rebootcount "1"; then
	fail_abort "${DMA_TESTNAME}-pre-test-reboot-check" "Unexpected reboot"
fi
check_dma_faults "${DMA_TESTNAME}-pre-test"

accel_config_test_run "${DMA_TESTNAME}" "/usr/libexec/accel-config/test/${IDXD_ACCFG_TEST}.sh"

if ! check_rebootcount "1"; then
	fail_abort "${DMA_TESTNAME}-post-test-reboot-check" "Unexpected reboot"
fi

grub_exit
cleanup_state

rstrnt-report-result "${DMA_TESTNAME}" "PASS"
